#!/bin/bash

BOLD='\e[1m'
NORMAL='\e[0m'

echo "*******************"
echo "* T&T Uninstaller *"
echo "*******************"
echo ""
echo "Tents and Trees est sur le point d'être désinstallé."
read -s -p "[ENTREE] pour continuer, [CRTL + C] pour quitter."

echo -e "\n\n${BOLD}Suppression des données du jeu...${NORMAL}"
sudo rm -f data/treeAndTents.db
echo -e "\n${BOLD}Suppression du launcher...${NORMAL}"
sudo rm -rf launcher
echo -e "\n${BOLD}Suppression du raccourci...${NORMAL}"
sudo rm -f tents-and-trees.desktop
echo -e "\n${BOLD}Suppression de l'icône du menu d'applications...${NORMAL}"
sudo rm -f /usr/share/applications/tents-and-trees.desktop