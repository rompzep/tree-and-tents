#!/bin/bash

BOLD='\e[1m'
NORMAL='\e[0m'

echo "*******************************"
echo "* T&T Ubuntu/Debian Installer *"
echo "*******************************"
echo ""
echo "Avant de procéder à l'installation, assurez-vous d'être :"
echo "=> connecté à internet."
echo "=> en possession des droits administrateur."
echo "=> placé dans le répertoire dans lequel vous souhaitez installer le jeu."
echo ""
echo "En cas de problème, n'hésitez pas à consulter le README ou visiter https://gitlab.com/Romreventon/tree-and-tents/tree/master."
echo ""
path=`pwd`
echo "T&T est sur le point d'être installé dans ${path}."
read -s -p "[ENTREE] pour continuer, [CRTL + C] pour quitter."

# Téléchargement et installation des paquets requis
echo -e "\n\n${BOLD}Mise à jour de la liste des paquets...${NORMAL}"
sudo apt-get -y update
echo -e "\n${BOLD}Installation de software-properties-common pour apt-add-repository...${NORMAL}"
sudo apt-get -y install software-properties-common
echo -e "\n${BOLD}Ajout du dépôt universe pour Ubuntu...${NORMAL}"
sudo add-apt-repository -y universe
echo -e "\n${BOLD}Re-mise à jour de la liste des paquets...${NORMAL}"
sudo apt-get -y update

echo -e "\n${BOLD}Installation du paquet build-essential...${NORMAL}"
sudo apt-get -y install build-essential

echo -e "\n${BOLD}Installation du paquet libsqlite3-dev...${NORMAL}"
sudo apt-get -y install libsqlite3-dev

echo -e "\n${BOLD}Installation du paquet libsdl2-dev...${NORMAL}"
sudo apt-get -y install libsdl2-dev

echo -e "\n${BOLD}Installation du paquet libsdl2-mixer-dev...${NORMAL}"
sudo apt-get -y install libsdl2-mixer-dev

echo -e "\n${BOLD}Installation de gobject-introspection pour le gem gtk3...${NORMAL}"
sudo apt-get install gobject-introspection libgirepository1.0-dev

echo -e "\n${BOLD}Installation de Ruby...${NORMAL}"
sudo apt-get -y install ruby-dev

# Gems
echo -e "\n${BOLD}Installation du gem rake...${NORMAL}"
sudo gem install rake

echo -e "\n${BOLD}Installation du gem gtk3...${NORMAL}"
sudo gem install gtk3

echo -e "\n${BOLD}Installation du gem sqlite3...${NORMAL}"
sudo gem install sqlite3

echo -e "\n${BOLD}Installation du gem ruby-sdl2...${NORMAL}"
sudo gem install ruby-sdl2

# Autorise l'éxécution des scripts Shell
echo -e "\n${BOLD}Autorisation d'éxécution des scripts Shell...${NORMAL}"
chmod +x `find -name '*.sh'`

# Génération de la RDoc
echo -e "\n${BOLD}Génération de la RDoc...${NORMAL}"
bash makedoc.sh

# Création du launcher et ajout dans le PATH
echo -e "\n${BOLD}Création du launcher portable...${NORMAL}"
mkdir -p launcher && echo -e "#!/bin/bash\n\nruby ${path}/src/gui/Main.rb" > launcher/tents-and-trees
chmod +x launcher/tents-and-trees
echo -e "\n${BOLD}Ajout de tents-and-trees dans le PATH...${NORMAL}"
echo -e "export PATH=$PATH:${path}/launcher" >> ~/.bashrc && source ~/.bashrc

# Création d'un raccourci du launcher dans le menu d'applications
echo -e "\n${BOLD}Création d'un raccourci dans le menu d'applications...${NORMAL}"
echo -e "
[Desktop Entry]\n
Type=Application\n
Name=Tents & Trees\n
Categories=Game\n
Exec=${path}/launcher/tents-and-trees
Icon=${path}/img/icon.png
Terminal=false" > tents-and-trees.desktop
chmod +x tents-and-trees.desktop
sudo cp tents-and-trees.desktop /usr/share/applications
