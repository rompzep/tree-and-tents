Réalisation du jeu "Des Tentes et des Arbres" en Ruby - Projet de Génie Logiciel de L3\
(Ruby, SQLite3, GTK3, CSS, Shell)

# Comment jouer ?
Reportez-vous au manuel d'utilisation pour les détails d'installation et de lancement. <br/>
(Bien que le jeu soit compatible pour macOS, il est recommandé de l'utiliser sous Ubuntu/Debian ou distributions dérivées.)

# Livrables
Accompagné du présent logiciel, l'archive contient le manuel d'utilisation, cahier des charges et documentation RDoc dans le dossier doc.

# Équipe de développement
- Romain Poupin (Romain.Poupin.Etu@univ-lemans.fr)
- Maxime Pocard (Maxime.Pocard.Etu@univ-lemans.fr)
- Maureen Gallois (Maureen.Gallois.Etu@univ-lemans.fr)
- Benjamin Nouvelière (Benjamin.Nouveliere.Etu@univ-lemans.fr)
- Andy Dinga (Andy.Dinga.Etu@univ-lemans.fr)